#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

// Golfed ever so slightly

std::uint64_t simulate(std::vector<std::uint64_t> ages, int days) {
  for (int day = 0; day < days; ++day) {
    std::rotate(ages.begin(), ages.begin() + 1, ages.end());
    ages.at(6) += ages.at(8);
  }

  return std::reduce(ages.begin(), ages.end());
}

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);
  if (!fin) return 2;

  std::vector<std::uint64_t> fishByAge(9, 0);
  std::string reading;
  while (std::getline(fin, reading, ',')) ++fishByAge[std::stoull(reading)];
  fin.close();

  std::cout << "Part 1: " << simulate(fishByAge, 80) << '\n';
  std::cout << "Part 2: " << simulate(fishByAge, 256) << '\n';
}
