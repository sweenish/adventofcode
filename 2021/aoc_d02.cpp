#include <fstream>
#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cerr << "Error opening file. Exiting...\n";
    return 2;
  }

  // Part 1 variables
  int horizontalLocationPart1 = 0;
  int depthPart1 = 0;
  // Part 2 variables
  int horizontalLocationPart2 = 0;
  int depthPart2 = 0;
  int aimPart2 = 0;
  std::string tmpDirection;
  int tmpInterval;
  while (fin >> tmpDirection && fin >> tmpInterval) {
    switch (tmpDirection[0]) {
      case 'f':
        horizontalLocationPart1 += tmpInterval;
        horizontalLocationPart2 += tmpInterval;
        depthPart2 += (aimPart2 * tmpInterval);
        break;
      case 'u':
        depthPart1 -= tmpInterval;
        aimPart2 -= tmpInterval;
        break;
      case 'd':
        depthPart1 += tmpInterval;
        aimPart2 += tmpInterval;
        break;
      default:
        std::cerr << "Here be dragons.\n";
    }
  }
  fin.close();

  std::cout << "Part 1: " << (horizontalLocationPart1 * depthPart1) << '\n';
  std::cout << "Part 2: " << (horizontalLocationPart2 * depthPart2) << '\n';
}