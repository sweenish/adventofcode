#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

struct BitCount {
  int ZEROS = 0;
  int ONES = 0;
};

enum class BitTypeToKeep { COMMON = 0, UNCOMMON = 1 };

std::vector<BitCount> count_bits(const std::vector<std::string>& values) {
  std::vector<BitCount> bitCounts(values[0].length());
  for (const auto& line : values) {
    for (std::size_t idx = 0; idx < line.length(); ++idx) {
      line[idx] == '1' ? ++bitCounts[idx].ONES : ++bitCounts[idx].ZEROS;
    }
  }

  return bitCounts;
}

unsigned int two_to_the_power(int exponent) {
  int val = 1;
  return exponent == 0 ? val : val << exponent;
}

unsigned int binary_to_int(const std::vector<int>& binaryVector) {
  unsigned int val = 0;

  int powerOfTwo = 0;
  for (auto it = binaryVector.crbegin(); it != binaryVector.crend();
       ++powerOfTwo, ++it) {
    if (*it == 1) val += two_to_the_power(powerOfTwo);
  }

  return val;
}

void flip_bits(std::vector<int>& binaryVector) {
  for (auto& i : binaryVector) {
    i = i == 1 ? 0 : 1;
  }
}

unsigned int filter(std::vector<std::string> values,
                    BitTypeToKeep bitTypeToKeep) {
  for (std::size_t i = 0; i < values[0].length(); ++i) {
    std::vector<BitCount> iterativeBitCount = count_bits(values);
    int bitToKeep;
    if (bitTypeToKeep == BitTypeToKeep::COMMON) {
      bitToKeep =
          iterativeBitCount[i].ONES >= iterativeBitCount[i].ZEROS ? 1 : 0;
    } else {
      bitToKeep =
          iterativeBitCount[i].ZEROS <= iterativeBitCount[i].ONES ? 0 : 1;
    }

    values.erase(std::remove_if(std::begin(values), std::end(values),
                                [i, bitToKeep](auto s) {
                                  return static_cast<int>(s[i] - '0') !=
                                         bitToKeep;
                                }),
                 values.end());

    if (values.size() == 1) break;
  }

  std::vector<int> valuesBinary;
  std::transform(std::begin(values[0]), std::end(values[0]),
                 std::back_inserter(valuesBinary),
                 [](auto c) { return static_cast<int>(c - '0'); });

  return binary_to_int(valuesBinary);
}

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cerr << "Error opening file. Exiting...\n";
    return 2;
  }

  std::vector<std::string> lines;
  std::copy(std::istream_iterator<std::string>(fin),
            std::istream_iterator<std::string>(), std::back_inserter(lines));
  fin.close();

  /*** Part 1 ***/
  auto bitCounts = count_bits(lines);

  std::vector<int> partOneReading;
  for (auto& i : bitCounts) {
    partOneReading.push_back(i.ONES > i.ZEROS ? 1 : 0);
  }

  unsigned int gamma = binary_to_int(partOneReading);
  flip_bits(partOneReading);
  unsigned int epsilon = binary_to_int(partOneReading);

  std::cout << "Part 1: " << (gamma * epsilon) << '\n';

  /*** Part 2 ***/
  auto oxygenGenerator(lines);
  auto co2Scrubber(lines);
  int oxygenLevel = filter(oxygenGenerator, BitTypeToKeep::COMMON);
  int co2Level = filter(co2Scrubber, BitTypeToKeep::UNCOMMON);
  std::cout << "Part 2: " << (oxygenLevel * co2Level) << '\n';
}