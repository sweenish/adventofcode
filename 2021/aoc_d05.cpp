#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace detail {
class Line {
  std::string m_line;

 public:
  friend std::istream& operator>>(std::istream& sin, Line& line) {
    return std::getline(sin, line.m_line);
  }

  operator std::string() const { return m_line; }
};
}  // namespace detail

struct XYPoint {
  int x;
  int y;
};

class Endpoints {
 public:
  Endpoints() = default;
  Endpoints(XYPoint first, XYPoint second)
      : m_start{first.y, first.x}, m_end{second.y, second.x} {
    if (m_start.second == m_end.second) {
      if (m_end.first < m_start.first)
        std::swap(m_start, m_end);  // Top to bottom
      return;
    }
    if (m_end.second < m_start.second)
      std::swap(m_start, m_end);  // Left to right
  }

  auto start() const { return m_start; }
  auto end() const { return m_end; }

 private:
  std::pair<int, int> m_start{0, 0};
  std::pair<int, int> m_end{0, 0};
};

Endpoints parse(std::string line) {
  std::string digits = "0123456789";
  XYPoint first;
  XYPoint second;

  std::size_t idx;
  first.x = std::stoi(line, &idx);
  line.erase(line.begin(), line.begin() + idx + 1);
  first.y = std::stoi(line, &idx);
  line.erase(line.begin(), line.begin() + idx + 1);
  idx = line.find_first_of(digits);
  line.erase(line.begin(), line.begin() + idx);
  second.x = std::stoi(line, &idx);
  line.erase(line.begin(), line.begin() + idx + 1);
  second.y = std::stoi(line, &idx);

  return {first, second};
}

class Grid {
 public:
  Grid(int i, int j) : m_grid(i, std::vector<int>(j, 0)) {}
  int& at(int i, int j) { return m_grid[i][j]; }

  void mark_line_Part1(const Endpoints& e) {
    if (e.start().first == e.end().first) {
      for (int i = e.start().second; i <= e.end().second; ++i) {
        ++m_grid[e.start().first][i];
      }
      return;
    }
    if (e.start().second == e.end().second) {
      for (int i = e.start().first; i <= e.end().first; ++i) {
        ++m_grid[i][e.start().second];
      }
      return;
    }
  }

  // Marking diagonals always starts at the leftmost point
  void mark_line_Part2(const Endpoints& e) {
    if (e.start().first != e.end().first &&
        e.start().second != e.end().second) {
      // Determin Direction
      if (e.start().first < e.end().first) {  // Down and right
        for (int i = e.start().first, j = e.start().second; i <= e.end().first;
             ++j, ++i) {
          ++m_grid[i][j];
        }
        return;
      }

      // Up and right
      for (int i = e.start().first, j = e.start().second; i >= e.end().first;
           ++j, --i) {
        ++m_grid[i][j];
      }
      return;
    }
  }

  auto dims() const {
    return std::make_tuple(static_cast<int>(m_grid.size()),
                           static_cast<int>(m_grid[0].size()));
  }

 private:
  std::vector<std::vector<int>> m_grid;
};

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cerr << "Error opening file.\n";
    return 2;
  }

  std::vector<std::string> fileLines;
  std::copy(std::istream_iterator<detail::Line>(fin),
            std::istream_iterator<detail::Line>(),
            std::back_inserter(fileLines));
  fin.close();

  std::vector<Endpoints> coordinates;
  std::transform(fileLines.begin(), fileLines.end(),
                 std::back_inserter(coordinates),
                 [](const auto& line) { return parse(line); });

  int globalMaxRowIdx = 0;
  int globalMaxColIdx = 0;
  for (auto i : coordinates) {
    int localRowMax =
        i.start().first > i.end().first ? i.start().first : i.end().first;
    int localColMax =
        i.start().second > i.end().second ? i.start().second : i.end().second;

    if (localRowMax > globalMaxRowIdx) globalMaxRowIdx = localRowMax;
    if (localColMax > globalMaxColIdx) globalMaxColIdx = localColMax;
  }

  Grid grid(globalMaxRowIdx + 1, globalMaxColIdx + 1);
  for (auto i : coordinates) {
    grid.mark_line_Part1(i);
  }

  auto count_intersections = [&grid] {
    auto [rows, cols] = grid.dims();
    int intersectionCounter = 0;
    for (auto i = 0; i < rows; ++i) {
      for (auto j = 0; j < cols; ++j) {
        if (grid.at(i, j) > 1) {
          ++intersectionCounter;
        }
      }
    }

    return intersectionCounter;
  };
  std::cout << "Part 1: " << count_intersections() << '\n';

  for (auto i : coordinates) {
    grid.mark_line_Part2(i);
  }
  std::cout << "Part 2: " << count_intersections() << '\n';
}