#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

int countDescents(const std::vector<int>& values) {
  int numDescents = 0;
  for (std::size_t idx = 1; idx < values.size(); ++idx) {
    if (values[idx - 1] < values[idx]) ++numDescents;
  }

  return numDescents;
}

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);
  if (!fin) {
    std::cerr << "File open Error.\n";
    return 2;
  }

  std::vector<int> readings;
  std::copy(std::istream_iterator<int>(fin), std::istream_iterator<int>(),
            std::back_inserter(readings));
  fin.close();

  std::cout << "Part 1: " << countDescents(readings) << '\n';

  const std::size_t windowSize = 3;
  std::vector<int> windowSums;
  for (int idx = 0; idx + windowSize - 1 < readings.size(); ++idx) {
    windowSums.push_back(readings[idx] + readings[idx + 1] + readings[idx + 2]);
  }

  std::cout << "Part 2: " << countDescents(windowSums) << '\n';
}