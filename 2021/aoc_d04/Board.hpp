#ifndef AOC_BOARD_HPP
#define AOC_BOARD_HPP

#include <array>
#include <iostream>
#include <tuple>
#include <vector>

namespace Bingo {
constexpr int size = 5;

struct Cell {
  int value = 0;
  bool marked = false;
};

struct Board {
  std::array<std::array<Cell, size>, size> board{};

  void mark(int row, int col);
  bool win() const;
  void print(std::ostream& sout = std::cout) const;
  void print_marked(std::ostream& sout = std::cout) const;
  std::tuple<int, int> find(int value) const;
  int tally(int lastNumber) const;

  Board() = default;
  Board(const std::vector<int>& values);

 private:
  bool row_win(int row) const;
  bool col_win(int col) const;
};
}  // namespace Bingo

#endif