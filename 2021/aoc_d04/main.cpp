#include <cctype>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "Board.hpp"
#include "Game.hpp"

std::vector<int> read_delimited(const std::string& line, char delim) {
  std::vector<int> callouts;
  std::string tmp;
  std::size_t idx = 0;
  while (idx < line.length()) {
    while (line[idx] != delim && idx < line.length()) {
      tmp.push_back(line[idx]);
      ++idx;
    }
    callouts.push_back(std::stoi(tmp));
    tmp.clear();
    ++idx;
  }

  return callouts;
}

void strip(std::string& line) {
  line.erase(std::unique(line.begin(), line.end(),
                         [](auto a, auto b) {
                           return std::isspace(a) && std::isspace(b);
                         }),
             line.end());

  auto beginning = line.find_first_not_of(" \n\t\r");
  auto ending = line.find_last_not_of(" \n\t\r");

  line = line.substr(beginning, ending - beginning + 1);
}

Bingo::Board make_board(std::vector<std::string>& raw) {
  std::string tmp;
  for (auto& i : raw) {
    tmp += i;
    tmp.push_back(' ');
  }

  return {read_delimited(tmp, ' ')};
}

int main(int argc, char* argv[]) {
  if (argc != 2) return 1;

  std::ifstream fin(argv[1]);

  std::string line;
  std::getline(fin, line);

  auto callouts = read_delimited(line, ',');

  Bingo::Game game;
  std::vector<std::string> rawBoard;
  while (std::getline(fin, line)) {
    if (line.length() == 0) {
      if (rawBoard.size() > 0) {
        game.add_board(make_board(rawBoard));
        rawBoard.clear();
      } else {
        continue;
      }
    } else {
      strip(line);
      rawBoard.push_back(line);
    }
  }
  fin.close();
  game.add_board(make_board(rawBoard));  // Solves loop off-by-one

  game.play_Part_1(callouts);
  game.play_Part_2(callouts);
}