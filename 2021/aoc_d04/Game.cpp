#include "Game.hpp"

#include <vector>

namespace Bingo {
void Game::add_board(Board b) { boards.push_back(b); }

void Game::play_Part_1(const std::vector<int>& callouts) {
  for (auto num : callouts) {
    // Mark boards
    for (auto& board : boards) {
      auto [x, y] = board.find(num);
      if (x != -1) {
        board.mark(x, y);
      }
    }

    // Check for a winner
    for (auto& board : boards) {
      if (board.win()) {
        board.print();
        board.print_marked();
        std::cout << "\nPart 1: " << board.tally(num) << '\n';
        return;
      }
    }
  }
}

void Game::play_Part_2(const std::vector<int>& callouts) {
  for (auto num : callouts) {
    // Mark boards
    for (auto& board : boards) {
      auto [x, y] = board.find(num);
      if (x != -1) {
        board.mark(x, y);
      }
    }

    // Check for a winner and remove it until there is one left
    for (auto board = boards.begin(); board != boards.end(); /**/) {
      if (board->win()) {
        if (boards.size() != 1) {
          board = boards.erase(board);
          // break;
        } else {
          board->print();
          board->print_marked();
          std::cout << "\nPart 2: " << board->tally(num) << '\n';
          return;
        }
      } else {
        ++board;
      }
    }
  }
}
}  // namespace Bingo