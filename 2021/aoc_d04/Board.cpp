#include "Board.hpp"

#include <iomanip>
#include <iostream>
#include <tuple>
#include <vector>

namespace Bingo {
void Board::mark(int row, int col) { board[row][col].marked = true; }

bool Board::win() const {
  for (int i = 0; i < size; ++i) {
    if (row_win(i)) return true;
    if (col_win(i)) return true;
  }

  return false;
}

void Board::print(std::ostream& sout) const {
  for (const auto& row : board) {
    for (const auto& col : row) {
      sout << std::setw(2) << col.value << ' ';
    }
    sout << '\n';
  }
  sout << '\n';
}

void Board::print_marked(std::ostream& sout) const {
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      if (board[i][j].marked) {
        sout << board[i][j].value << ' ';
      }
    }
  }
  sout << '\n';
}

std::tuple<int, int> Board::find(int value) const {
  for (std::size_t i = 0; i < size; ++i) {
    for (std::size_t j = 0; j < size; ++j) {
      if (board[i][j].value == value) {
        return std::make_tuple(static_cast<int>(i), static_cast<int>(j));
      }
    }
  }

  return std::make_tuple(-1, -1);
}

int Board::tally(int lastNumber) const {
  int score = 0;
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      if (!board[i][j].marked) {
        score += board[i][j].value;
      }
    }
  }

  return score * lastNumber;
}

Board::Board(const std::vector<int>& values) {
  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      board[i][j] = Cell{values.at(i * size + j), false};
    }
  }
}

bool Board::row_win(int row) const {
  for (int i = 0; i < size; ++i) {
    if (board[row][i].marked == false) return false;
  }

  return true;
}

bool Board::col_win(int col) const {
  for (int i = 0; i < size; ++i) {
    if (board[i][col].marked == false) return false;
  }

  return true;
}
}  // namespace Bingo