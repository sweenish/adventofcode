#ifndef AOC_GAME_HPP
#define AOC_GAME_HPP

#include <vector>

#include "Board.hpp"

namespace Bingo {
class Game {
 public:
  Game() = default;
  void add_board(Board b);
  void play_Part_1(const std::vector<int>& callouts);
  void play_Part_2(const std::vector<int>& callouts);

 private:
  std::vector<Board> boards{};
};
}  // namespace Bingo

#endif